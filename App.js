import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

import Picture from './components/Picture';
import User from './components/User';
import BlinkingText from './components/BlinkingText';
import StyledTextBlock from './components/StyledTextBlock';
import WordCounter from './components/WordCounter';
import AlertButton from './components/AlertButton';
import Touchables from './components/Touchables';
import UserList from './components/UserList';

export default function App() {
    return (
        <View>
            <ScrollView style={{ height: 400 }}>
                <View style={styles.container}>
                    <Picture />
                    <Text>React Native Application</Text>
                </View>
                <View style={styles.container}>
                    <WordCounter />
                    <User name='Ivan' login='@Ivan2020' />
                    <StyledTextBlock />
                </View>
                <View style={styles.container}>
                    <AlertButton alertText='Alert text' />
                </View>
                <View style={styles.container}>
                    <Touchables />
                </View>
            </ScrollView>
            <UserList />
            <View style={styles.container}>
                <BlinkingText text='Blinking text' />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 50,
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
