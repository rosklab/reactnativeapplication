import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, TouchableHighlight, TouchableOpacity,
    TouchableNativeFeedback, TouchableWithoutFeedback, View
} from 'react-native';

export default class Touchables extends Component {
    onPressButton() {
        alert('You tapped the button!')
    }

    onLongPressButton() {
        alert('You long-pressed the button!')
    }


    render() {
        return (
            <View>
                <TouchableHighlight
                    onPress={this.onPressButton}
                    onLongPress={this.onLongPressButton}
                    underlayColor='white'>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>TouchableHighlight</Text>
                    </View>
                </TouchableHighlight>
                <TouchableOpacity onPress={this.onPressButton} onLongPress={this.onLongPressButton}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>TouchableOpacity</Text>
                    </View>
                </TouchableOpacity>
                <TouchableNativeFeedback
                    onPress={this.onPressButton}
                    onLongPress={this.onLongPressButton}
                    background={Platform.OS === 'android' ? TouchableNativeFeedback.SelectableBackground() : ''}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>TouchableNativeFeedback {Platform.OS !== 'android' ? '(Android only)' : ''}</Text>
                    </View>
                </TouchableNativeFeedback>
                <TouchableWithoutFeedback
                    onPress={this.onPressButton}
                    onLongPress={this.onLongPressButton}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>TouchableWithoutFeedback</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        width: 200,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2196F3',
        marginTop: 10,
        marginBottom: 10
    },
    buttonText: {
        textAlign: 'center',
        color: 'white'
    }
});
