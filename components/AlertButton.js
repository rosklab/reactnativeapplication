import React, { Component } from 'react';
import { Button } from 'react-native';

export default class AlertButton extends Component {
    render() {
        return (
            <Button
                title='Press Me'
                onPress={() => alert(this.props.alertText)}
            />
        );
    }
}
