import React, { Component } from 'react';
import { Image } from 'react-native';

export default class Picture extends Component {
    render() {
        let picture = require('./../images/react-native.png');
        return (
            <Image source={picture} style={{ width: 100, height: 100 }} />
        );
    }
}
