import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class User extends Component {
    render() {
        return (
            <View style={this.props.style}>
                <Text>User name: {this.props.name}</Text>
                <Text>User login: {this.props.login}</Text>
            </View>
        );
    }
}