import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';

import User from './User'

const styles = StyleSheet.create({
    item: {
        padding: 10,
        alignItems: 'center',
        alignContent: 'center'
    }
})

export default class UserList extends Component {
    render() {
        return (
            <FlatList
                data={[
                    { key: 'Ivan', login: '@Ivan2020' },
                    { key: 'Julie', login: '@Jul' },
                    { key: 'John', login: '@Johny' },
                    { key: 'Alex', login: '@Alex_10' },
                    { key: 'Bob', login: '@Bob' },
                    { key: 'Tom', login: '@TomCat' },
                    { key: 'Sam', login: '@Sam972' },
                    { key: 'Ted', login: '@Ted' },
                    { key: 'Logon', login: '@Logon' }
                ]}
                renderItem={({ item }) => {
                    return (
                        <User name={item.key} login={item.login} style={styles.item}/>
                    );
                }}
            />
        );
    }
}
