import React, { Component } from 'react';
import { Text } from 'react-native';

export default class BlinkingText extends Component {
    state = { isShowingText: true };

    componentDidMount() {
        setInterval(() => {
            this.setState(previousState => {
                return { isShowingText: !previousState.isShowingText }
            });
        }, 1000);
    }

    render() {
        if (!this.state.isShowingText) {
            return null;
        }

        return (
            <Text>{this.props.text}</Text>
        );
    }
}
