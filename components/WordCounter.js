import React, { Component } from 'react';
import { Text, TextInput, View } from 'react-native';

export default class WordCounter extends Component {
    state = {
        text: '',
        wordCount: 0
    };

    render() {
        return (
            <View>
                <TextInput
                    style={{ height: 40 }}
                    placeholder="Type here you words"
                    onChangeText={(text) => this.setState(
                        {
                            text: text,
                            wordCount: text.split(/\s+/).filter(word => word).length
                        }
                    )}
                    value={this.state.text}
                />
                <Text>
                    {`Word count: ${this.state.wordCount}`}
                </Text>
            </View>
        );
    }
}
