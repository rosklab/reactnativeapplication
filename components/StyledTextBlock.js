import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';

const styles = StyleSheet.create({
    boldGreen: {
        color: 'green',
        fontWeight: 'bold'
    },
    blue: {
        color: 'blue'
    },
    limitedBlackBackground: {
        backgroundColor: 'black',
        width: 100,
        height: 50
    }
});

export default class StyledTextBlock extends Component {
    render() {
        return (
            <View>
                <Text style={styles.blue}>just blue</Text>
                <Text style={styles.boldGreen}>just boldGreen</Text>
                <Text style={[styles.boldGreen, styles.blue]}>boldGreen, then blue</Text>
                <Text style={[styles.blue, styles.boldGreen]}>blue, then boldGreen</Text>
                <Text style={[styles.blue, styles.limitedBlackBackground]}>just blue</Text>
            </View>
        );
    }
}
